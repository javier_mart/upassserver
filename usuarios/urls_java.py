# -*- encoding: utf-8 -*-

__author__ = 'brian'


from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required


urlpatterns = patterns('', url(r'^login/$', 'usuarios.views_java.login'),
                       url(r'^logout/$', 'usuarios.views_java.logout'),
                       url(r'^registro/$', 'usuarios.views_java.registro'),
                       url(r'^get_perfil/$', 'usuarios.views_java.get_perfil'),
                       url(r'^comprobar_token/$', 'usuarios.views_java.comprobar_token'),
                       url(r'^cambiar_pass/$', 'usuarios.views_java.cambiar_pass'),
                       )