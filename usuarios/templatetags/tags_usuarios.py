# -*- coding: utf-8 -*-

from django import template

register = template.Library()








@register.simple_tag(name='get_nombre_archivo')
def get_nombre_archivo(url):

    u = url.split('/')[-1]

    return u

@register.simple_tag(name='get_tipo_archivo')
def get_tipo_archivo(url):

    u = url.split('/')[-1]
    exten = u.split('.')[-1]
    return exten

@register.simple_tag(name='get_url_server_galeria')
def get_url_server(url):

    cortes = url.find("/galeria")+1
    urlfinal = url[0:cortes]
    return urlfinal

@register.simple_tag(name='get_url_server_mensajes')
def get_url_server(url):

    cortes = url.find("/mensajes")+1
    urlfinal = url[0:cortes]
    return urlfinal
    
@register.filter(name='is_nivel_c')
def is_nivel_c(nivel):
    if nivel == "c":
        return True
    else:
        return False
        
@register.filter(name='is_nivel_b')
def is_nivel_b(nivel):
    if nivel == "b":
        return True
    else:
        return False
        
@register.filter(name='is_nivel_a')
def is_nivel_a(nivel):
    if nivel == "a":
        return True
    else:
        return False
        
@register.filter(name='is_nivel_s')
def is_nivel_s(nivel):
    if nivel == "s":
        return True
    else:
        return False
        
