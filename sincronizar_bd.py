__author__ = 'brian'
import os
from configuracion import settings


os.system("python manage.py syncdb")

for a in settings.INSTALLED_APPS_MIGRATE:
    os.system("rm "+a+"/migrations/0*")
    os.system("ls "+a+"/migrations/")
    os.system("python manage.py schemamigration "+a+" --initial")
    os.system("python manage.py migrate "+a)