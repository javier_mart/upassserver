__author__ = 'brian'
import os
import commands
from configuracion import settings


for a in settings.INSTALLED_APPS_MIGRATE:
    print "migrando "+a
    res = commands.getoutput("python manage.py schemamigration "+a+" --auto")
    print res

    if res.find("You cannot use")>=0:
        res = commands.getoutput("python manage.py schemamigration "+a+" --initial")
    os.system("python manage.py migrate "+a)