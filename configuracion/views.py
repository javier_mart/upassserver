# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.shortcuts import render, render_to_response
from django.utils.functional import wraps
from django.template.context import RequestContext
from annoying.functions import get_object_or_None
from contrasenas.models import Palabra
from random import randint
from contrasenas.models import Contrasena
import random
import string

import django.http as http


@login_required()
def inicio(request):
    usuario = request.user
    return render_to_response('principal.html',context_instance=RequestContext(request,{'usuario':usuario}))


@login_required()
def perfil(request):
    usuario = request.user
    ncont = Contrasena.objects.filter(usuario=usuario).count()
    ncontc = Contrasena.objects.filter(usuario=usuario, nivel='c').count()
    ncontb = Contrasena.objects.filter(usuario=usuario, nivel='b').count()
    nconta = Contrasena.objects.filter(usuario=usuario, nivel='a').count()
    nconts = Contrasena.objects.filter(usuario=usuario, nivel='s').count()
    
    levelp = 0
    
    if ncont > 0:
        media = ncont / 2
        masalto = 0
        if ncontc > media:
            masalto = ncontc
        if ncontb > media:
            if ncontb > masalto:
                masalto = ncontb
        if nconta > media:
            if nconta > masalto:
                masalto = nconta
        if nconts > media:
            if nconts > masalto:
                masalto = nconts
        
        if masalto == ncontc:
            levelp = 1
        if masalto == ncontb:
            levelp = 2
        if masalto == nconta:
            levelp = 3
        if masalto == nconts:
            levelp = 4
            
        
    return render_to_response('perfil.html',context_instance=RequestContext(request,{'usuario':usuario, 'ncont':ncont,
        'ncontc':ncontc, 'ncontb':ncontb, 'nconta':nconta, 'nconts':nconts, 'levelp':levelp
    }))

def generador(request):
    contrasenagen = "error"
    count = Palabra.objects.all().count()
    contrasenagen = Palabra.objects.all()
    contr = contrasenagen[randint(0, count-1)]
    
    pal1 = ""
    i = 0
    while i < 9:
        pal1 += str(contrasenagen[randint(0, count-1)])
        pal1 += str(randint(0, 100))
        i += 1
    # -----------------------------------------------------------
    pal2 = ""
    i = 0
    while i < 30:
        pal2 += str(randint(0, 100))
        i += 1
    # -----------------------------------------------------------
    pal3 = ""
    i = 0
    size = 70
    chars = string.ascii_uppercase + string.ascii_lowercase + string.digits
    pal3 = ''.join(random.choice(chars) for x in range(size))
    
    
    return render_to_response('generador.html',context_instance=RequestContext(request,{'pal1':pal1, 'pal2':pal2, 'pal3':pal3}))

# copiar todo eso sin el usuario

'''
@login_required()
def main(request):
    usuario = Usuario.objects.get(usuario=request.user)
    return render_to_response('principal.html',context_instance=RequestContext(request,{'usuario':usuario}))
'''