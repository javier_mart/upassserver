# -*- coding: utf-8 -*-

"""
Dreams Apps Creative

Django settings for configuracion project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""
from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
PROJECT_PATH = os.path.dirname(__file__)

FILE_UPLOAD_DIRECTORY_PERMISSIONS = 0777

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '64n8mi+=gsy3fwq(%gbe9rxd#_25=s*-%$-((ue5&z8dnyjgs2'
# SECRET_KEY = 't7*h9vyvmbuzg9!jic2ejgh=9el+-_f8e@#zpchv#u&e@x!t%&'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (

    'django.contrib.staticfiles',
    'suit',  # If you deploy your project with Apache or Debug=False dont forget to run ./manage.py collectstatic
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'mathfilters',
    'floppyforms',
    'bootstrapform',
    'south',
    'usuarios',
    'contrasenas'



)
# añadimos aqui las aplicaciones que necesitamos migrar
INSTALLED_APPS_MIGRATE = (


    'usuarios',
    'contrasenas'


)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATE_DIRS = [os.path.join(BASE_DIR, 'templates')]

"""
para poder instalar con apache hay que descomentar static root y comentar
los staticfiles_dirs hacer python manage.py collectstatic y dejar comoe staba
"""
#STATIC_ROOT = os.path.join(BASE_DIR, 'static')

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'dajaxice.finders.DajaxiceFinder',
)

TEMPLATE_CONTEXT_PROCESSORS = TCP + (
    'django.core.context_processors.request',
    'configuracion.processors.comun',
)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader',
)

ROOT_URLCONF = 'configuracion.urls'

WSGI_APPLICATION = 'configuracion.wsgi.application'

LANGUAGE_CODE = 'es-es'

SESSION_COOKIE_AGE = 18000 #seg
SESSION_SAVE_EVERY_REQUEST = True

SITE_ID = 1

TIME_ZONE = 'Europe/Madrid'
USE_I18N = True
USE_L10N = True
USE_TZ = True



STATIC_URL = '/static/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'static/media')
MEDIA_URL = 'static/media/'

MEDIA_FILES = MEDIA_ROOT + '/archivos/'
MEDIA_FILES_URL = MEDIA_URL + 'archivos/'

MEDIA_VIDEO = MEDIA_ROOT + '/video/'
MEDIA_VIDEO_URL = MEDIA_URL+'video/'

MEDIA_AUDIO = MEDIA_ROOT + '/audio/'
MEDIA_AUDIO_URL = MEDIA_URL +'audio/'

MEDIA_IMAGE = MEDIA_ROOT + '/imagenes/'
MEDIA_IMAGE_URL = MEDIA_URL +'imagenes/'

GALERIA_ROOT = MEDIA_ROOT + '/galeria/'
GALERIA_URL = MEDIA_URL + 'galeria/'

LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/usuarios/administradores/'

# Add to your settings file
CONTENT_TYPES = ['image', 'video', 'audio']
# 2.5MB - 2621440
# 5MB - 5242880
# 10MB - 10485760
# 20MB - 20971520
# 50MB - 5242880
# 100MB 104857600
# 250MB - 214958080
# 500MB - 429916160
MAX_UPLOAD_SIZE = "5242880"

try:
  import local_settings
except ImportError:
  print """
    -------------------------------------------------------------------------
    You need to create a local_settings.py file which needs to contain at least
    database connection information.
    -------------------------------------------------------------------------
    """
  import sys
  sys.exit(1)
else:
  # Import any symbols that begin with A-Z. Append to lists any symbols that
  # begin with "EXTRA_".
  import re
  for attr in dir(local_settings):
    match = re.search('^EXTRA_(\w+)', attr)
    if match:
      name = match.group(1)
      value = getattr(local_settings, attr)
      try:
        globals()[name] += value
      except KeyError:
        globals()[name] = value
    elif re.search('^[A-Z]', attr):
      globals()[attr] = getattr(local_settings, attr)

