# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url



from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns('',

    url(r'^$', 'configuracion.views.inicio', name='inicio'),
    url(r'^generador/$', 'configuracion.views.generador', name='generador'),
    url(r'^perfil/$', 'configuracion.views.perfil', name='perfil'),
    url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'login.html'}),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'template_name': 'login.html'}, name='logout'),
    

    url(r'^admin/', include(admin.site.urls)),
    url(r'^usuarios/', include('usuarios.urls')),
    url(r'^usuarios/java/', include('usuarios.urls_java')),
    url(r'^contrasenas/java/', include('contrasenas.urls_java')),
    url(r'^contrasenas/', include('contrasenas.urls')),



)

#if settings.DEBUG:
    #import debug_toolbar
    #urlpatterns += patterns('',
    #    url(r'^__debug__/', include(debug_toolbar.urls)),
    #)
