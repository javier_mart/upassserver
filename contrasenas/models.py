# -*- encoding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User
from django.forms import ModelForm
from django.db.models.signals import post_save
import os

PROJECT_PATH = os.path.dirname("__file__")


class Contrasena(models.Model):
    usuario = models.ForeignKey(User)
    nombre = models.CharField(max_length=30)
    usuarioweb = models.CharField(max_length=2000)
    nivel = models.CharField(max_length=1)
    contrasena = models.CharField(max_length=2000)

    def __unicode__(self):
        return u"%s_%s" % (self.usuario, self.nombre)

class Palabra(models.Model):
    palabra = models.CharField(max_length=100)
    
    def __unicode__(self):
        return u"%s" % (self.palabra)