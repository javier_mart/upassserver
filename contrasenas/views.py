# -*- encoding: utf-8 -*-


import forms, models
from django.views.generic import ListView, FormView, DeleteView,CreateView, UpdateView
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, get_object_or_404
import django.http as http
from django.core.urlresolvers import reverse
from django.utils.encoding import smart_str
from django.contrib.auth.models import User
from annoying.functions import get_object_or_None
from utilidades import enviarmail,contrasena
from django.utils.timezone import utc
from models import Contrasena


from django.http import HttpResponse
from django.contrib import auth
import json
from django.views.decorators.csrf import csrf_exempt

import xml.etree.ElementTree as ET

class ContrasenasList(CreateView):
    model = Contrasena
    context_object_name = 'contrasenas'
    template_name = 'contrasenas/contrasenas_list.html'

    def get(self, request, *args, **kwargs):
        contrasenas = Contrasena.objects.filter(usuario=request.user).order_by('-id')
        return render(request, self.template_name, {'contrasenas': contrasenas})
        
class ContrasenaCreate(CreateView):
    template_name = 'contrasenas/contrasena_create.html'
    form_class = forms.ContrasenaForm

    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            cont = form.save(commit=False)

            # Asigna el usuario actual automaticamente
            cont.usuario = request.user
            
            # ////////////////////////////////////////////////////////////////////
            # Calcula el tipo de nivel
            password = form['contrasena'].value()
            password = ""+password
            
            
            if password is None or password == "":
                nivel = "c"
            
            passwordStrength = 0
            if len(password) > 4:
                passwordStrength += 1
            # minimal pw length of 4
            
            maxUpper = 0
            for c in password:
                if c.isupper() and maxUpper < 6:
                    passwordStrength += 1
                    maxUpper += 1
            # lower and upper case
            
            if len(password) > 8:
                passwordStrength += 1
            # good pw length of 9+
            
            numDigits = sum(c.isdigit() for c in password)
            if numDigits > 0:
                passwordStrength += 1
            # contains digits and non-digits
            
            if passwordStrength == 0:
                nivel = "c"
            elif passwordStrength < 3:
                nivel = "c"
            elif passwordStrength < 5:
                nivel = "b"
            elif passwordStrength < 7:
                nivel = "a"
            elif passwordStrength > 7:
                nivel = "s"
            
            cont.nivel = nivel
            # ////////////////////////////////////////////////////////////////////
            # Encripta la contraseña
            dicc = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','ñ','o','p','q','r','s','t','u','v','w','x','y','z',
            'A','B','C','D','E','F','G','H','I','J','K','L','M','N','Ñ','O','P','Q','R','S','T','U','V','W','X','Y','Z',
            '0','1','2','3','4','5','6','7','8','9']
            i = 0
            pasos = cont.usuario.pk
            aux = list(password)
            while i < len(aux):
                if aux[i] in dicc:
                    pos = dicc.index(aux[i])
                    lon = pos + pasos
                    if lon > len(dicc)-1:
                        lon = lon%len(dicc)
                    aux[i] = dicc[lon]
                    i += 1
                else:
                    aux[i] = aux[i]
                    i += 1
            
            # while i < len(aux):
            #     pos = dicc.index(aux[i])
            #     lon = pos - pasos
                
            #     if lon < 0:
            #         lon = lon%len(dicc)
            #     aux[i] = dicc[lon]
                
            #     i += 1

            cont.contrasena = ''.join(aux)
            cont.save()
            
            return http.HttpResponseRedirect(reverse('contrasenas_list'))
        return render(request, self.template_name, {'form': form})
        
class ContrasenaDelete(DeleteView):
    template_name = 'contrasenas/contrasena_delete.html'
    form_class = forms.PassForm

    def get(self, request, *args, **kwargs):
        contrasena = get_object_or_404(Contrasena, pk=self.kwargs['pk'])
        form = self.form_class()
        return render(request, self.template_name, {'form':form, 'contrasena':contrasena})

    def post(self, request, *args, **kwargs):
        contrasena = get_object_or_404(Contrasena, pk=self.kwargs['pk'])
        form = self.form_class(request.POST)
        password = form['passw'].value()
        correcto = request.user.check_password(password)
        if correcto:
            contrasena.delete()
            return http.HttpResponseRedirect(reverse('contrasenas_list'))
        else:
            return render(request, self.template_name, {'form':form, 'contrasena':contrasena})
        
class ContrasenaEdit(UpdateView):
    model = Contrasena
    form_class = forms.ContrasenaForm
    template_name = 'contrasenas/contrasena_edit.html'

    def get(self, request, *args, **kwargs):
        contrasena= get_object_or_404(Contrasena, pk=self.kwargs['pk'])
        form = self.form_class(instance=contrasena)
        return render(request, self.template_name, {'form': form, 'contrasena':contrasena})

    def post(self, request, *args, **kwargs):
        contrasena = get_object_or_404(Contrasena, pk=self.kwargs['pk'])
        form = self.form_class(request.POST,request.FILES, instance=contrasena)

        if form.is_valid():
            cont = form.save(commit=False)

            # Asigna el usuario actual automaticamente
            cont.usuario = request.user
            # Calcula el tipo de nivel
            password = form['contrasena'].value()
            password = ""+password
            
            if password is None or password == "":
                nivel = "c"
            
            passwordStrength = 0
            if len(password) > 4:
                passwordStrength += 1
            # minimal pw length of 4
            
            maxUpper = 0
            for c in password:
                if c.isupper() and maxUpper < 6:
                    passwordStrength += 1
                    maxUpper += 1
            # lower and upper case
            
            if len(password) > 8:
                passwordStrength += 1
            # good pw length of 9+
            
            numDigits = sum(c.isdigit() for c in password)
            if numDigits > 0:
                passwordStrength += 1
            # contains digits and non-digits
            
            if passwordStrength == 0:
                nivel = "c"
            elif passwordStrength < 3:
                nivel = "c"
            elif passwordStrength < 5:
                nivel = "b"
            elif passwordStrength < 7:
                nivel = "a"
            elif passwordStrength > 7:
                nivel = "s"
            
            cont.nivel = nivel
            # ////////////////////////////////////////////////////////////////////
            # Encripta la contraseña
            dicc = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','ñ','o','p','q','r','s','t','u','v','w','x','y','z',
            'A','B','C','D','E','F','G','H','I','J','K','L','M','N','Ñ','O','P','Q','R','S','T','U','V','W','X','Y','Z',
            '0','1','2','3','4','5','6','7','8','9']
            i = 0
            pasos = cont.usuario.pk
            aux = list(password)
            while i < len(aux):
                if aux[i] in dicc:
                    pos = dicc.index(aux[i])
                    lon = pos + pasos
                    if lon > len(dicc)-1:
                        lon = lon%len(dicc)
                    aux[i] = dicc[lon]
                    i += 1
                else:
                    aux[i] = aux[i]
                    i += 1
            
            cont.contrasena = ''.join(aux)
            
            cont.save()
            return http.HttpResponseRedirect(reverse('contrasenas_list'))

        return render(request, self.template_name, {'form':form, 'contrasena': contrasena})

@csrf_exempt
def descifra_pass(request):
    
    pk = request.POST['pk']
    try:
        contrasena = get_object_or_None(Contrasena, pk=pk)
    except Exception as e:
        contrasena=None
        
    if contrasena is not None:
        password = contrasena.contrasena
        # Desencripta la contraseña
        dicc = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','ñ','o','p','q','r','s','t','u','v','w','x','y','z',
        'A','B','C','D','E','F','G','H','I','J','K','L','M','N','Ñ','O','P','Q','R','S','T','U','V','W','X','Y','Z',
        '0','1','2','3','4','5','6','7','8','9']
        i = 0
        pasos = contrasena.usuario.pk
        aux = list(password)
        while i < len(aux):
            if aux[i] in dicc:
                pos = dicc.index(aux[i])
                lon = pos - pasos
                if lon < 0:
                    lon = lon%len(dicc)
                aux[i] = dicc[lon]
                i += 1
            else:
                aux[i] = aux[i]
                i += 1

        password = ''.join(aux)
        # // plis
        response_data = {'result': 'ok','pass':password}
    else:
        response_data = {'result': 'error','pass':"error"}
    
    
    return http.HttpResponse(json.dumps(response_data),content_type="application/json")
    

class SuperDelete(DeleteView):
    template_name = 'contrasenas/super_delete.html'
    form_class = forms.PassForm

    def get(self, request, *args, **kwargs):
        usuario = get_object_or_404(User, pk=self.kwargs['pk'])
        form = self.form_class()
        return render(request, self.template_name, {'form':form, 'usuario':usuario})

    def post(self, request, *args, **kwargs):
        usuario = get_object_or_None(User, pk=self.kwargs['pk'])
        form = self.form_class(request.POST)
        password = form['passw'].value()
        correcto = request.user.check_password(password)
        if correcto:
            contrasenas = Contrasena.objects.filter(usuario=usuario)
            
            raiz = ET.Element('UPass')
            
            contrs = []
            contr = []
            
            for c in contrasenas:
                head = ET.SubElement(raiz, 'U')
                ET.SubElement(head, 'Nombre').text = c.nombre
                ET.SubElement(head, 'Usuario').text = c.usuarioweb
                
                # Desencripta la contraseña
                dicc = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','ñ','o','p','q','r','s','t','u','v','w','x','y','z',
                'A','B','C','D','E','F','G','H','I','J','K','L','M','N','Ñ','O','P','Q','R','S','T','U','V','W','X','Y','Z',
                '0','1','2','3','4','5','6','7','8','9']
                i = 0
                pasos = usuario.pk
                aux = list(c.contrasena)
                while i < len(aux):
                    if aux[i] in dicc:
                        pos = dicc.index(aux[i])
                        lon = pos - pasos
                        if lon < 0:
                            lon = lon%len(dicc)
                        aux[i] = dicc[lon]
                        i += 1
                    else:
                        aux[i] = aux[i]
                        i += 1
        
                pawor = ''.join(aux)
                # -------------------------
                
                ET.SubElement(head, 'Pass').text = pawor
                
            contrs = {'UPass': contr}
            
            tree = ET.ElementTree(raiz)
            tree.write("Upass.xml")          
            # tree.write("Upass_"+str(usuario.pk)+"_"+usuario.username+".xml")          
            
            contrasenas.delete()
            return http.HttpResponseRedirect(reverse('contrasenas_list'))
            # return HttpResponse(open("Upass_"+str(usuario.pk)+"_"+usuario.username+".xml").read())
        else:
            return render(request, self.template_name, {'form':form, 'usuario':usuario})