# -*- encoding: utf-8 -*-

import django.http as http
from annoying.functions import get_object_or_None
from django.views.decorators.csrf import csrf_exempt
import json

from contrasenas.models import Contrasena
from usuarios.views_java import comprobar_usuario, get_userdjango_by_token


@csrf_exempt
def get_contrasenas(request):
    print "Enviando las contraseñas"

    try:
        datos = json.loads(request.POST['data'])

        if comprobar_usuario(datos):
            userdjango = get_userdjango_by_token(datos)
            contrasenas = Contrasena.objects.filter(usuario=userdjango)
            arraycontrasenas= []
            for c in contrasenas:
                cn = {"pk": c.pk, "nombre": c.nombre ,"usuarioweb": c.usuarioweb,"nivel":c.nivel,"contrasena":c.contrasena}
                arraycontrasenas.append(cn)

            response_data = {"result": "ok", 'message': 'ok', "contrasenas": arraycontrasenas, "pk_usuario":userdjango.pk}
        else:
            response_data = {'result': 'error', 'message': 'Usuario no logueado'}

        return http.HttpResponse(json.dumps(response_data), content_type="application/json")

    except Exception as e:
        response_data = {'errorcode': 'error0001', 'result': 'error', 'message': str(e)}
        return http.HttpResponse(json.dumps(response_data), content_type="application/json")

@csrf_exempt
def enviar_contrasena(request):
    print "Guardando la contraseña"

    try:
        datos = json.loads(request.POST['data'])
        nombre = datos.get("nombre")
        usuarioweb = datos.get("usuarioweb")
        nivel = datos.get("nivel")
        contrasena = datos.get("contrasena")

        if comprobar_usuario(datos):
            userdjango = get_userdjango_by_token(datos)
            con = Contrasena(usuario=userdjango, nombre=nombre, usuarioweb=usuarioweb, nivel=nivel, contrasena=contrasena)
            if con is not None:
                con.save()
                response_data = {"result": "ok", 'message': 'ok'}
            else:
                response_data = {"result": "error", 'message': 'Contrasena no guardada'}
        else:
            response_data = {'result': 'error', 'message': 'Usuario no logueado'}

        return http.HttpResponse(json.dumps(response_data), content_type="application/json")

    except Exception as e:
        response_data = {'errorcode': 'error0002', 'result': 'error', 'message': str(e)}
        return http.HttpResponse(json.dumps(response_data), content_type="application/json")

@csrf_exempt
def editar_contrasena(request):
    print "Editando la contraseña"

    try:
        datos = json.loads(request.POST['data'])
        pk = datos.get("pk")
        nombre = datos.get("nombre")
        usuarioweb = datos.get("usuarioweb")
        nivel = datos.get("nivel")
        contrasena = datos.get("contrasena")

        if comprobar_usuario(datos):
            userdjango = get_userdjango_by_token(datos)
            con = get_object_or_None(Contrasena, pk=pk)
            if con is not None:
                con.nombre = nombre
                con.usuarioweb = usuarioweb
                con.nivel = nivel
                con.contrasena = contrasena
                con.save()
                response_data = {"result": "ok", 'message': 'ok'}
            else:
                response_data = {"result": "error", 'message': 'Contrasena no editada'}
        else:
            response_data = {'result': 'error', 'message': 'Usuario no logueado'}

        return http.HttpResponse(json.dumps(response_data), content_type="application/json")

    except Exception as e:
        response_data = {'errorcode': 'error0003', 'result': 'error', 'message': str(e)}
        return http.HttpResponse(json.dumps(response_data), content_type="application/json")

@csrf_exempt
def eliminar_contrasena(request):
    print "Eliminando la contraseña"

    try:
        datos = json.loads(request.POST['data'])
        pk = datos.get("pk")

        if comprobar_usuario(datos):
            userdjango = get_userdjango_by_token(datos)
            con = get_object_or_None(Contrasena, pk=pk)
            if con is not None:
                con.delete()
                response_data = {"result": "ok", 'message': 'ok'}
            else:
                response_data = {"result": "error", 'message': 'Contrasena no eliminada'}
        else:
            response_data = {'result': 'error', 'message': 'Usuario no logueado'}

        return http.HttpResponse(json.dumps(response_data), content_type="application/json")

    except Exception as e:
        response_data = {'errorcode': 'error0004', 'result': 'error', 'message': str(e)}
        return http.HttpResponse(json.dumps(response_data), content_type="application/json")
