
from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required

from contrasenas import views

urlpatterns = patterns('',
          url (r'^contrasenas_list/$', login_required(views.ContrasenasList.as_view()), name='contrasenas_list'),
          url (r'^contrasena_create/$', login_required(views.ContrasenaCreate.as_view()), name='contrasena_create'),
          url (r'^contrasena_delete/(?P<pk>\d+)$', login_required(views.ContrasenaDelete.as_view()), name='contrasena_delete'),
          url (r'^contrasena_edit/(?P<pk>\d+)$', login_required(views.ContrasenaEdit.as_view()), name='contrasena_edit'),
          url (r'^super_delete/(?P<pk>\d+)$', login_required(views.SuperDelete.as_view()), name='super_delete'),
          url(r'^descifra_pass/$', 'contrasenas.views.descifra_pass', name='descifra_pass'),
)