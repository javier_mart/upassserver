from django.contrib import admin
from contrasenas import models

admin.site.register(models.Contrasena)
admin.site.register(models.Palabra)
