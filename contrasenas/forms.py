#-*- encoding: utf-8 -*-

__author__ = 'javi'

import floppyforms as forms
from models import Contrasena


class ContrasenaForm(forms.ModelForm):

    class Meta:
        model = Contrasena
        exclude = {
            'usuario',
            'nivel'
        }
        widgets = {
        }

class PassForm(forms.Form):
    passw = forms.CharField(widget=forms.PasswordInput, label='Contraseña de usuario')